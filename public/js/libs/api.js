class Api {
    constructor(){
        this.token = getCookie('token') || window.token || window.document.token || 'open';
        this.url   = "http://localhost:3000/api";
    }

    getStatus(callback){
        this.api({
            url     : this.url + "/workers/status",
            method  : "get",
            data    : {},
            success : callback
        });
    }

    /**
     * Get list of tasks
     * @param params
     * @param params.page
     * @param callback
     */
    getList(params, callback){
        this.api({
            url     : this.url + "/tasks",
            method  : "GET",
            data    : params,
            success : callback
        });
    }

    addTask(params, callback){
        this.api({
            url     : this.url + '/tasks',
            method  : 'post',
            data    : params,
            success : callback
        });
    }

    addManyTasks(params, callback){
        this.api({
            url     : this.url + "/tasks/multy",
            method  : "POST",
            data    : params,
            success : callback
        })
    }

    /**
     *
     * @param params
     * @param params.url
     * @param params.method
     * @param params.data
     * @param params.success
     * @param callback
     */
    api(params,callback){
        $.ajax({
            url      : params.url,
            method   : params.method,
            data     : _plus({token : this.token},params.data),
            dataType : 'json',
            success  : params.success,
            error    : () => {setTimeout(this.api.bind(this, params, callback), 5000)},
            timeout  : 60000
        });
    }
}


let _plus = function (input, objct) {
    let out = {};

    let inoutKeys = Object.keys(input);
    let addKeys   = Object.keys(objct);

    inoutKeys.forEach(key => {
        out[key] = input[key];
    });

    addKeys.forEach(key => {
        out[key] = objct[key];
    });

    input = out;

    return out;
};

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}