class App extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            list: null,
            addTask: null,
            addTest: null,
            status: null
        };
    }

    componentWillMount() {}

    componentWillReceiveProps() {}

    showAddTask() {
        this.setState({ addTest: React.createElement(AddTask, null) }, () => {});
    }

    showAddTest() {
        this.setState({ addTest: React.createElement(AddTest, null) }, () => {});
    }

    render() {
        return React.createElement(
            "div",
            { className: "container" },
            React.createElement(
                "div",
                { className: "row" },
                React.createElement(
                    "div",
                    { className: "col-9" },
                    React.createElement(TasksList, null),
                    React.createElement("hr", null),
                    React.createElement(
                        "div",
                        { className: "btn-group w-100" },
                        React.createElement(
                            "button",
                            { className: "btn btn-primary w-100", onClick: this.showAddTask.bind(this) },
                            "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043E\u0434\u0438\u043D \u0442\u0430\u043A\u0441"
                        ),
                        React.createElement(
                            "button",
                            { className: "btn btn-primary w-100", onClick: this.showAddTest.bind(this) },
                            "\u0414\u043E\u0431\u043E\u0430\u0432\u0438\u0442\u044C \u0442\u0435\u0441\u0442"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col-3" },
                    React.createElement(Status, null)
                )
            ),
            this.state.addTask,
            this.state.addTest
        );
    }
}

ReactDOM.render(React.createElement(App, null), document.getElementById('root'));
//# sourceMappingURL=app.js.map
