class Status extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            cards: null
        };

        this.api = new Api();
    }

    componentWillMount() {
        this.fillCards();
    }

    componentWillReceiveProps() {
        // this.fillCards();
    }

    fillCards() {
        this.api.getStatus(data => {
            if (data.code === 200) {
                let cards = [];

                for (let worker in data.data) {
                    cards.push(React.createElement(StatusBlock, { title: "Проесс номер " + worker, worker: data.data[worker], key: worker }));
                    cards.push(React.createElement("hr", null));
                }
                if (cards.length) this.setState({ cards: cards }, () => {});else this.setState({ cards: React.createElement(Error, { title: "\u041E\u0448\u0438\u0431\u043A\u0430", message: "\u041F\u0440\u043E\u0446\u0435\u0441\u0441\u044B \u0438\u0441\u043F\u043E\u043B\u043D\u0438\u0442\u0435\u043B\u0438 \u043D\u0435\u0434\u043E\u0441\u0442\u0443\u043F\u043D\u044B" }) }, () => {});
            } else {
                this.setState({ cards: React.createElement(Error, { title: "\u041E\u0448\u0438\u0431\u043A\u0430", message: data.data.message }) }, () => {});
            }

            setTimeout(this.fillCards.bind(this), 1000);
        });
    }

    render() {
        return React.createElement(
            "div",
            null,
            this.state.cards
        );
    }
}

class StatusBlock extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            title: p.title,
            worker: p.worker
        };
    }

    componentWillReceiveProps(p) {
        this.setState(p, () => {});
    }

    render() {
        return React.createElement(
            "div",
            { className: "card" },
            React.createElement(
                "div",
                { className: "card-body" },
                React.createElement(
                    "div",
                    { className: "card-title" },
                    React.createElement(
                        "h6",
                        { className: "rounded p-1 bg-primary" },
                        this.state.title
                    )
                ),
                React.createElement(
                    "div",
                    { className: "row card-text" },
                    React.createElement(
                        "div",
                        { className: "col" },
                        React.createElement(
                            "span",
                            {
                                style: { color: this.state.worker.status ? "red" : "green" } },
                            this.state.worker.status ? "Занаят" : "Свободен"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col" },
                        "\u0417\u0430\u0432\u0435\u0440\u0448\u0438\u043B ",
                        this.state.worker.done
                    )
                )
            )
        );
    }
}

class AddTask extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            show: true
        };

        this.inputData = null;

        this.api = new Api();
    }

    componentWillMount() {}

    componentWillReceiveProps() {
        this.show();
    }

    onChangeInput(e) {
        this.inputData = e.target.value;
    }

    add() {
        $('#loader').addClass('d-block');
        this.api.addTask({
            input: JSON.stringify({ value: this.inputData })
        }, () => {
            $('#loader').removeClass('d-block');
            this.hide();
        });
    }

    show() {
        this.setState({ show: true }, () => {});
    }

    hide() {
        this.setState({ show: false }, () => {});
    }

    render() {
        return React.createElement(
            "div",
            { className: "modal", style: { display: this.state.show ? "block" : "" } },
            React.createElement(
                "div",
                { className: "modal-dialog", role: "document" },
                React.createElement(
                    "div",
                    { className: "modal-content" },
                    React.createElement(
                        "div",
                        { className: "modal-header" },
                        React.createElement(
                            "h5",
                            { className: "modal-title" },
                            "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043E\u0434\u043D\u0443 \u0437\u0430\u0434\u0430\u0447\u0443"
                        ),
                        React.createElement(
                            "button",
                            { type: "button", className: "close", onClick: this.hide.bind(this), "aria-label": "Close" },
                            React.createElement(
                                "span",
                                { "aria-hidden": "true" },
                                "\xD7"
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "modal-body" },
                        React.createElement(
                            "form",
                            null,
                            React.createElement(
                                "div",
                                { className: "form-group" },
                                React.createElement(
                                    "label",
                                    null,
                                    "\u0412\u0445\u043E\u0434\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435"
                                ),
                                React.createElement("input", { className: "form-control", type: "number", onChange: this.onChangeInput.bind(this) })
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "modal-footer" },
                        React.createElement(
                            "button",
                            { type: "button", className: "btn btn-primary", onClick: this.add.bind(this) },
                            "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C"
                        ),
                        React.createElement(
                            "button",
                            { type: "button", className: "btn btn-secondary", onClick: this.hide.bind(this) },
                            "\u0417\u0430\u043A\u0440\u044B\u0442\u044C"
                        )
                    )
                )
            )
        );
    }
}

class AddTest extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            show: true,
            error: null
        };

        this.count = 0;
        this.api = new Api();
    }

    componentWillMount() {
        this.show();
    }

    componentWillReceiveProps() {
        this.show();
    }

    onChangeInput(e) {
        this.count = e.target.value;
    }

    add() {
        $('#loader').addClass('d-block');

        let tasks = [];
        for (let i = 0; i < this.count; i++) {
            tasks.push({ value: Math.random() });
        }

        this.api.addManyTasks({ input: JSON.stringify(tasks) }, data => {
            if (data.code === 200) {
                $('#loader').removeClass('d-block');
                this.hide();
                window.scrollTo(0, 0);
            } else {
                this.setState({ error: React.createElement(Error, { title: "\u041E\u0448\u0438\u0431\u043A\u0430", message: err }) }, () => {});
            }
        });
    }

    show() {
        this.setState({ show: true }, () => {});
    }

    hide() {
        this.setState({ show: false }, () => {});
        $('#loader').removeClass('d-block');
    }

    render() {
        return React.createElement(
            "div",
            { className: "modal", style: { display: this.state.show ? "block" : "" } },
            React.createElement(
                "div",
                { className: "modal-dialog", role: "document" },
                React.createElement(
                    "div",
                    { className: "modal-content" },
                    React.createElement(
                        "div",
                        { className: "modal-header" },
                        React.createElement(
                            "h5",
                            { className: "modal-title" },
                            "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043E\u0434\u043D\u0443 \u0442\u0435\u0441\u0442 (\u043D\u0435 \u0440\u0435\u043A\u043E\u043C\u0435\u043D\u0434\u0443\u044E \u0434\u043E\u0431\u0430\u0432\u043B\u044F\u0442\u044C \u0431\u043E\u043B\u044C\u0448\u0435 2000 \u0437\u0430\u0434\u0430\u0447)"
                        ),
                        React.createElement(
                            "button",
                            { type: "button", className: "close", onClick: this.hide.bind(this), "aria-label": "Close" },
                            React.createElement(
                                "span",
                                { "aria-hidden": "true" },
                                "\xD7"
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "modal-body" },
                        React.createElement(
                            "form",
                            null,
                            React.createElement(
                                "div",
                                { className: "form-group" },
                                React.createElement(
                                    "label",
                                    null,
                                    "\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0442\u0435\u0441\u0442\u043E\u0432\u044B\u0439 \u0437\u0430\u0434\u0430\u0447"
                                ),
                                React.createElement("input", { className: "form-control", type: "number", onChange: this.onChangeInput.bind(this) })
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "modal-footer" },
                        React.createElement(
                            "button",
                            { type: "button", className: "btn btn-primary", onClick: this.add.bind(this) },
                            "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C"
                        ),
                        React.createElement(
                            "button",
                            { type: "button", className: "btn btn-secondary", onClick: this.hide.bind(this) },
                            "\u0417\u0430\u043A\u0440\u044B\u0442\u044C"
                        )
                    )
                )
            ),
            this.state.error
        );
    }
}

class TasksList extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            tasks: null,
            pageBlock: null,
            page: 0
        };

        this.api = new Api();
    }

    componentWillMount() {
        this.filltask();
        this.setCurr();
    }

    componentWillReceiveProps() {
        this.filltask();
        this.setCurr();
    }

    filltask() {
        this.api.getList({ page: this.state.page }, data => {
            if (data.code === 200) {
                if (data.data.length) {
                    this.setState({ tasks: [] }, () => {
                        this.setState({ tasks: data.data.map(task => React.createElement(ListItem, { task: task, key: "t-" + task.id })) }, () => {});
                    });
                } else this.setState({ tasks: React.createElement(Error, { title: "", message: "\u041D\u0435\u0442\u0443 \u0437\u0430\u0434\u0430\u0447" }) }, () => {});
            } else {
                this.setState({ tasks: React.createElement(Error, { title: "\u041E\u0448\u0438\u0431\u043A\u0430", message: data.data.message }) }, () => {});
            }
        });
    }

    nextPage() {
        this.setState({ page: this.state.page + 1 }, () => {
            this.setCurr();
            this.filltask();
        });
    }

    prevPage() {
        this.state.page ? this.setState({ page: this.state.page - 1 }, () => {
            this.setCurr();
            this.filltask();
        }) : 0;
    }

    setCurr() {
        this.setState({ pageBlock: React.createElement(Pages, { next: this.nextPage.bind(this), prev: this.prevPage.bind(this), curr: this.state.page }) }, () => {});
    }

    render() {
        return React.createElement(
            "div",
            null,
            React.createElement(
                "button",
                { className: "btn btn-primary", onClick: this.filltask.bind(this) },
                "\u041E\u0431\u043D\u043E\u0432\u0438\u0442\u044C"
            ),
            React.createElement(
                "div",
                { className: "row" },
                React.createElement(
                    "div",
                    { className: "col-1" },
                    "ID"
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    "\u0412\u0445\u043E\u0434\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435"
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    "\u0412\u044B\u0445\u043E\u0434\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435"
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    "\u0421\u043E\u0437\u0434\u0430\u043D"
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    "\u0417\u0430\u043F\u0443\u0449\u0435\u043D"
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    "\u0412\u044B\u043F\u043E\u043B\u043D\u0435\u043D"
                )
            ),
            React.createElement(
                "ul",
                { className: "list-group" },
                this.state.tasks
            ),
            React.createElement("hr", null),
            React.createElement(
                "div",
                { className: "w-100" },
                this.state.pageBlock
            )
        );
    }
}

class ListItem extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            task: p.task
        };
    }

    render() {
        return React.createElement(
            "li",
            { className: "list-group-item" },
            React.createElement(
                "div",
                { className: "row" },
                React.createElement(
                    "div",
                    { className: "col-1" },
                    React.createElement(
                        "span",
                        null,
                        this.state.task.id
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    React.createElement(
                        "span",
                        null,
                        this.state.task.input_data
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    React.createElement(
                        "span",
                        null,
                        this.state.task.output_data
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    React.createElement(
                        "small",
                        null,
                        new Date(Date.parse(this.state.task.created_at)).toLocaleTimeString()
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    React.createElement(
                        "small",
                        null,
                        new Date(Date.parse(this.state.task.started_at)).toLocaleTimeString()
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col" },
                    React.createElement(
                        "small",
                        null,
                        new Date(Date.parse(this.state.task.finished_at)).toLocaleTimeString()
                    )
                )
            )
        );
    }
}

class Pages extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            next: p.next,
            prev: p.prev,
            curr: p.curr
        };
    }

    componentWillMount() {}

    componentWillReceiveProps(p) {
        this.setState({ curr: p.curr }, () => {});
    }

    render() {
        return React.createElement(
            "nav",
            { "aria-label": "Page navigation example" },
            React.createElement(
                "ul",
                { className: "pagination" },
                React.createElement(
                    "li",
                    { className: "page-item" },
                    React.createElement(
                        "a",
                        { className: "page-link", onClick: this.state.prev },
                        "Previous"
                    )
                ),
                React.createElement(
                    "li",
                    { className: "page-item" },
                    React.createElement(
                        "a",
                        { className: "page-link" },
                        this.state.curr + 1
                    )
                ),
                React.createElement(
                    "li",
                    { className: "page-item" },
                    React.createElement(
                        "a",
                        { className: "page-link", onClick: this.state.next },
                        "Next"
                    )
                )
            )
        );
    }
}

class Error extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            title: p.title,
            message: p.message,

            show: true
        };
    }

    componentWillMount() {}

    componentWillReceiveProps(p) {
        this.setState({ title: p.title, message: p.message, show: true }, () => {});
    }

    show() {
        this.setState({ show: true }, () => {});
    }

    hide() {
        this.setState({ show: false }, () => {});
    }

    close() {
        this.state.show ? this.hide() : this.show();
    }

    render() {
        return React.createElement(
            "div",
            { className: "alert alert-warning alert-dismissible fade " + (this.state.show ? "show" : ""), role: "alert" },
            React.createElement(
                "button",
                { type: "button", className: "close", onClick: this.close.bind(this) },
                React.createElement(
                    "span",
                    { "aria-hidden": "true" },
                    "\xD7"
                )
            ),
            React.createElement(
                "strong",
                null,
                this.state.title
            ),
            " ",
            this.state.message
        );
    }
}
//# sourceMappingURL=components.js.map
