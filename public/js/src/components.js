class Status extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            cards : null
        };

        this.api = new Api();
    }

    componentWillMount(){
        this.fillCards();
    }

    componentWillReceiveProps(){
        // this.fillCards();
    }

    fillCards(){
        this.api.getStatus(data => {
            if(data.code === 200){
                let cards = [];

                for(let worker in data.data){
                    cards.push(<StatusBlock title={"Проесс номер "+worker} worker={data.data[worker]} key={worker}/>);
                    cards.push(<hr/>);
                }
                if(cards.length)
                    this.setState({cards : cards},()=>{});
                else
                    this.setState({cards : <Error title="Ошибка" message="Процессы исполнители недоступны" />},()=>{});
            }else {
                this.setState({cards : <Error title="Ошибка" message={data.data.message} />},()=>{});
            }

            setTimeout(this.fillCards.bind(this), 1000);
        });
    }

    render(){
        return (
            <div>
                {this.state.cards}
            </div>
        )
    }
}

class StatusBlock extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            title : p.title,
            worker : p.worker
        };
    }

    componentWillReceiveProps(p){
        this.setState(p,()=>{});
    }

    render(){
        return (
            <div className="card">
                <div className="card-body">
                    <div className="card-title">
                        <h6 className="rounded p-1 bg-primary">{this.state.title}</h6>
                    </div>
                    <div className="row card-text">
                        <div className="col">
                            <span
                                style={{color: this.state.worker.status ? "red" : "green"}}>{this.state.worker.status ? "Занаят" : "Свободен"}</span>
                        </div>
                        <div className="col">Завершил {this.state.worker.done}</div>
                    </div>
                </div>
            </div>
        )
    }
}

class AddTask extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            show : true,
        };

        this.inputData = null;

        this.api = new Api();
    }

    componentWillMount(){

    }

    componentWillReceiveProps(){
        this.show();
    }

    onChangeInput(e){
        this.inputData = e.target.value;
    }

    add(){
        $('#loader').addClass('d-block');
        this.api.addTask({
            input : JSON.stringify({value :this.inputData})
        }, ()=>{
            $('#loader').removeClass('d-block');
            this.hide();
        });
    }

    show(){
        this.setState({show : true},()=>{});
    }

    hide(){
        this.setState({show : false},()=>{});
    }

    render(){
        return (
            <div className="modal" style={{display : this.state.show ? "block" : ""}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Добавить одну задачу</h5>
                            <button type="button" className="close" onClick={this.hide.bind(this)} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="form-group">
                                    <label>Входные данные</label>
                                    <input className="form-control" type="number" onChange={this.onChangeInput.bind(this)}/>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.add.bind(this)}>Добавить</button>
                            <button type="button" className="btn btn-secondary" onClick={this.hide.bind(this)}>Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class AddTest extends React.Component {
    constructor(p){
        super(p);
        
        this.state = {
            show : true,
            error : null
        };

        this.count = 0;
        this.api = new Api();
    }
    
    componentWillMount() {
        this.show();
    }

    componentWillReceiveProps(){
        this.show();
    }

    onChangeInput(e){
        this.count = e.target.value;
    }

    add(){
        $('#loader').addClass('d-block');

        let tasks = [];
        for(let i = 0; i < this.count; i++){
            tasks.push({value:Math.random()});
        }
        
        this.api.addManyTasks({input : JSON.stringify(tasks)}, data => {
            if(data.code === 200){
                $('#loader').removeClass('d-block');
                this.hide();
                window.scrollTo(0, 0);
            }else{
                this.setState({error : <Error title="Ошибка" message={err}/>},()=>{});
            }
        });
    }

    show(){
        this.setState({show : true},()=>{});
    }

    hide(){
        this.setState({show : false},()=>{});
        $('#loader').removeClass('d-block');
    }

    render(){
        return (
            <div className="modal" style={{display : this.state.show ? "block" : ""}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Добавить одну тест (не рекомендую добавлять больше 2000 задач)</h5>
                            <button type="button" className="close" onClick={this.hide.bind(this)} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="form-group">
                                    <label>Количество тестовый задач</label>
                                    <input className="form-control" type="number" onChange={this.onChangeInput.bind(this)}/>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.add.bind(this)}>Добавить</button>
                            <button type="button" className="btn btn-secondary" onClick={this.hide.bind(this)}>Закрыть</button>
                        </div>
                    </div>
                </div>
                {this.state.error}
            </div>
        );
    }
}

class TasksList extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            tasks : null,
            pageBlock : null,
            page : 0
        };
        
        this.api = new Api();
    }

    componentWillMount(){
        this.filltask();
        this.setCurr();
    }

    componentWillReceiveProps(){
        this.filltask();
        this.setCurr();
    }
    
    filltask(){
        this.api.getList({page : this.state.page}, data => {
            if(data.code === 200){
                if(data.data.length){
                    this.setState({tasks : []},()=>{
                        this.setState({tasks : data.data.map(task => <ListItem task={task} key={"t-"+task.id}/>)},()=>{});
                    });
                }else
                    this.setState({tasks : <Error title="" message="Нету задач"/>},()=>{});
            }else {
                this.setState({tasks: <Error title="Ошибка" message={data.data.message}/>},()=>{});
            }
        })
    }

    nextPage(){
        this.setState({page : this.state.page+1},()=>{
            this.setCurr();
            this.filltask();
        });
    }

    prevPage(){
        this.state.page ? this.setState({page: this.state.page-1},()=>{
            this.setCurr();
            this.filltask();
        }) : 0;
    }

    setCurr(){
        this.setState({pageBlock: <Pages next={this.nextPage.bind(this)} prev={this.prevPage.bind(this)} curr={this.state.page}/>},()=>{});
    }


    render(){
        return (
            <div>
                <button className="btn btn-primary" onClick={this.filltask.bind(this)}>Обновить</button>
                <div className="row">
                    <div className="col-1">ID</div>
                    <div className="col">Входные данные</div>
                    <div className="col">Выходные данные</div>
                    <div className="col">Создан</div>
                    <div className="col">Запущен</div>
                    <div className="col">Выполнен</div>
                </div>
                <ul className="list-group">{this.state.tasks}</ul>
                <hr/>
                <div className="w-100">
                    {this.state.pageBlock}
                </div>
            </div>
        );
    }
}

class ListItem extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            task : p.task
        };
    }

    render(){
        return (
            <li className="list-group-item">
                <div className="row">
                    <div className="col-1">
                        <span>{this.state.task.id}</span>
                    </div>
                    <div className="col">
                        <span>{this.state.task.input_data}</span>
                    </div>
                    <div className="col">
                        <span>{this.state.task.output_data}</span>
                    </div>
                    <div className="col">
                        <small>{new Date(Date.parse(this.state.task.created_at)).toLocaleTimeString()}</small>
                    </div>
                    <div className="col">
                        <small>{new Date(Date.parse(this.state.task.started_at)).toLocaleTimeString()}</small>
                    </div>
                    <div className="col">
                        <small>{new Date(Date.parse(this.state.task.finished_at)).toLocaleTimeString()}</small>
                    </div>
                </div>
            </li>
        );
    }
}

class Pages extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            next : p.next,
            prev : p.prev,
            curr : p.curr
        };
    }

    componentWillMount(){

    }

    componentWillReceiveProps(p){
        this.setState({curr: p.curr}, ()=>{});
    }

    render(){
        return (
            <nav aria-label="Page navigation example">
            <ul className="pagination">
                <li className="page-item"><a className="page-link" onClick={this.state.prev}>Previous</a></li>
                <li className="page-item"><a className="page-link">{this.state.curr + 1}</a></li>
                <li className="page-item"><a className="page-link" onClick={this.state.next}>Next</a></li>
            </ul>
        </nav>)
    }
}

class Error extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            title   : p.title,
            message : p.message,

            show : true
        };
    }

    componentWillMount(){

    }

    componentWillReceiveProps(p){
        this.setState({title : p.title, message: p.message, show : true},()=>{});
    }

    show(){
        this.setState({show : true}, () =>{})
    }

    hide(){
        this.setState({show : false},()=>{});
    }

    close(){
        this.state.show ? this.hide() : this.show();
    }

    render(){
        return (
            <div className={"alert alert-warning alert-dismissible fade "+(this.state.show ? "show" : "")} role="alert">
                <button type="button" className="close" onClick={this.close.bind(this)}>
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{this.state.title}</strong> {this.state.message}
            </div>
        )
    }
}