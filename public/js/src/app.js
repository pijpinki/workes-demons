class App extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            list : null,
            addTask : null,
            addTest : null,
            status   : null
        };
    }

    componentWillMount(){

    }

    componentWillReceiveProps(){

    }

    showAddTask(){
        this.setState({addTest: <AddTask />},()=>{});
    }

    showAddTest(){
        this.setState({addTest: <AddTest />},()=>{});
    }

    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col-9">
                        <TasksList/>
                        <hr/>
                        <div className="btn-group w-100">
                            <button className="btn btn-primary w-100" onClick={this.showAddTask.bind(this)}>Добавить один такс</button>
                            <button className="btn btn-primary w-100" onClick={this.showAddTest.bind(this)}>Добоавить тест</button>
                        </div>
                    </div>

                    <div className="col-3">
                        <Status />
                    </div>
                </div>
                {this.state.addTask}
                {this.state.addTest}
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));