let amqp        = require('amqplib/callback_api');
let WorkerClass = require('./workerClass');

let IDE       = 0;
let BUSY      = 1;
let status    = IDE;
let taskCount = 0;

let Name   = null;
let Global = "Global";
let Tasks  = "tasks";
let Master = "master";


module.exports.start = function (name) {
    console.log(`init worker ${name}`);
    Name = name;

    amqp.connect('amqp://lmkxmovw:EHJyzCUF5WoSY6RK4e-DO5rBEk17HVed@lark.rmq.cloudamqp.com/lmkxmovw', (err, conn) => {

        if (!err) {
            conn.createChannel((err, ch) => {
                console.log('chanel', err);
                this.main(conn, ch);
            });
        }
    });
};


module.exports.main = function (conn, ch) {
    let Worker = new WorkerClass(setStatus, updateCount);

    ch.assertQueue(Global, {durable: false});
    ch.assertQueue(Tasks);
    ch.assertQueue(Master , {durable: false});
    ch.consume(Global, reciveGlobal, {noAck: true});
    ch.consume(Tasks, reciveTask);



    function reciveGlobal(input) {
        let data = JSON.parse(input.content.toString());
        console.log('recive data', data);

        setTimeout(()=>{ch.ack(input)}, 50);
    }

    function reciveTask(input) {
        let data = JSON.parse(input.content.toString());
        console.log('recive data', data);

        data.msg = input;
        data.ch  = ch;
        Worker.add(data);
    }

    function reciveMe(data) {

    }

    function send(to,data) {
        data = new Buffer(JSON.stringify(data));
        ch.sendToQueue(to, data);
    }


    function statusCheck() {
        send(Master, {type : 'status', data:{id:Name, status : status, done : taskCount}});
    }


    function updateCount(c) {
        taskCount += c;
    }

    function setStatus(s) {
        status = s;
    }
    setInterval(statusCheck, 1000);

};


// debug