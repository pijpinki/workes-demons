let cluster    = require('cluster');
let worker     = require('./worker');

// Таймер для проверки количества воркеров и остановки мастера
let shutdownInterval = null;
    if (cluster.isMaster) {
        // Запускаем воркеры
        for(let i = 0; i < 4; i++){
            startWorker(Math.random());
        }
    } else {
        // Инициализируем воркер
        let name = process.env.WORKER_NAME;
        worker.start(name);
        process.on('message', message => {
            if ('shutdown' === message) {
                if (worker) {
                    worker.stop();
                } else {
                    process.exit();
                }
            }
        });
    }
// Shutdown
process.on('SIGTERM', shutdownCluster);
process.on('SIGINT', shutdownCluster);

function startWorker(name) {
    let worker = cluster.fork({WORKER_NAME: name}).on('online', () => {
    }).on('exit', status => {
        if ((worker.exitedAfterDisconnect || worker.suicide) === true || status === 0) {
          } else {
            startWorker(name);
        }
    });
}

// Метод остановки кластера
function shutdownCluster() {
    if (cluster.isMaster) {
        clearInterval(shutdownInterval);
        if (_.size(cluster.workers) > 0) {
            // Посылаем сигнал останова каждому воркеру\
            _.each(cluster.workers, worker => {
                try {
                    worker.send('shutdown');
                } catch (err) {
                }
            });
            // Ожидаем останов всех воркеров
            shutdownInterval = setInterval(() => {
                if (_.size(cluster.workers) === 0) {
                    process.exit();
                }
            }, config.shutdownInterval);
        } else {
            process.exit();
        }
    }
}