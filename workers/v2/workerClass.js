let EventEmitter = require('events');
let Tasks        = require('../../database/tasks');



class WorkerClass {
    constructor(setStatus, updateCounter){
        this.status = 0;

        this.callback      = null;
        this.setStatus     = setStatus;
        this.updateCounter = updateCounter;

        this.query = [];

        this.listener();
    }

    workAll(){
        let _this = this;
        r();
        function r() {
            _this.workOne.call(_this,_this.query[0], () => {
                _this.query.shift();
                if(_this.query.length) r(); else {
                    _this.status = 0;
                    _this.setStatus(0);
                    _this.listener();
                }
            });
        }
    }

    workOne(task, callback){
        Tasks.setStarted({ids:[task.id]},()=> {
            setTimeout(() => {
                task.output_data = JSON.parse(task.input_data).value + 100;
                Tasks.setOutput({id: task.id, output_data: task.output_data}, (err) => {
                    this.updateCounter(1);
                    task.ch.ack(task.msg);
                    callback()
                });
            }, 100);
        });
    }

    listener(){
        let _this = this;
        l();
        function l() {
            if(_this.query.length > 0) {
                if(!_this.status){
                    _this.workAll();
                    _this.status = 1;
                    _this.setStatus(1);
                }else {
                    setTimeout(l, 100);
                }
            }
            else setTimeout(l,100);
        }
    }

    add(task){
        this.query.push(task);
    }
}

module.exports = WorkerClass;