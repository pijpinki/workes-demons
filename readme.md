# Читай меня

## Описание
workers-demos Pavel Pasieka 14.08.1998

Проект написан для прохождения собеседования при приеме на работу.
И не несет коммерческой выгоды. Может использоваться в учебных целях.

Здесь демонстрируется работа с базой данных Mysql, создания кластера node.js

Работа с микросервисами и обеспечение стабильности передачи заданий для микросервисов

Проект имеет скрипты для самотестирования, и графический интерфейс.

Графический интерфейс доступен по адресу http://localhost:3000

## Запуск главного процесса 

> terminal 
```
#npm start
```
>pm2
```
#pm2 bin/www
```

## Запуск микросервисов daemons 

> terminal
```
#node workers/v2/start.js
```
> pm2
```
#pm2 workers/v2/start.js
```

## Запуск тестов
```
#mocha test
```

## Зависимости
+ Node.js V8.2.0 + https://nodejs.org/en/download/"
+ Mysql https://www.mysql.com/downloads/ 
+ Internet connection

## Возможные проблемы и их решеия

+ Не запускаються тесты

Установить mocha глобоально 
```
#npm install -g mocha
```

+ Не запускаеться сервер ошибка err connect to amqp...

Проверить соедение с интеренетом если не помогло то связаться со мной 
скайп skype:/usescr

+ Не работает графический интерфейс, не отображаються елементы

Зайти на UI через другой браузер 

## TODO 
+ Перевести на sokcet мониторинг
+ Добавить добавления множественных задач о