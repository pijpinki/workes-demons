let EventEmitter = require('events');
let Tasks        = require('../database/tasks');

class WorkerClass extends EventEmitter {
    constructor(){
        super();

        // 0 IDE, 1 BUSY
        this.status = 0;
        this.query  = [];
        this.count  = 0;
        this.done   = [];
    }

    start(){
        if(!this.status) {
            this.status = 1;

            this.markStaretdDB(this.startLoop.bind(this));
            this.emit('start');
        }
    }

    startLoop(err,data){
        if(err) {
            this.status = 0;
            this.emit('crash');
        }else {
            this.loop();
        }
    }

    loop(){

        function r(i) {
            let q = this.query[i];
            q.output_data = this.math(JSON.parse(q.input_data).value);
            this.done.push(q);
            this.count--;

            Tasks.setOutput({id : q.id, output_data : q.output_data}, err => {
                i < this.query.length ? setTimeout(r.bind(this,i+1),100) : done.call(this);
            });

        }


        function done() {
            this.emit('stop', this.done);
            this.status = 0;
            this.done  = [];
            this.query = [];

        }
        this.query.length > 0 ? r.call(this, 0) : 0;
    }

    addToQuery(data){
        this.query.push(data);
        this.count = this.query.length;
    }

    getStatus(){
        return this.status;
    }

    getRemaingn(){
        return this.count;
    }

    markStaretdDB(callback){
        let ids = [];
        this.query.forEach(q => ids.push(q.id));
        Tasks.setStarted({ids : ids}, callback);
    }


    math(a){
        for(let i = 0; i < 2**4; i++){
            a++;
            a--;
        }
        a *= Math.random() * 100;
        return a;
    }
}

module.exports = WorkerClass;