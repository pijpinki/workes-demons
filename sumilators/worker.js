let mq = require('./mq');
let client = mq.createClient();
let WorkerClass  = require('./workerClass');

let Work  = new WorkerClass();

client.on('started', data =>{
    console.log('from worker, i am stared');
});


// В оригинале будет client.on('message');
client.on('1', data => {
   if(data.type === "query"){
       data.data.forEach(d => Work.addToQuery(d));
       Work.start();
   }
   if(data.type === "status"){
       let status = Work.getStatus();
       let count  = Work.getRemaingn();

       client.send('master', {type : status, worker:{id : 1, status: status, remeaning : count}});
   }
});

// В оригинале паралельно подключилмся в каналу global и будем слушать message
client.on('global', data => {
    if(data.type === 'status'){
        client.send('master', {type : "started", worker : {id : 1, status : 1, busy : 0, remeaning :0}});
    }
    if(data.type === 'state'){
        client.send('master', {type : "state", worker : {id : 1, status : 1, busy : Work.getStatus(), remeaning : Work.getRemaingn()}});
    }
});


Work.on('start', () => {
   client.send('master', {type: 'busy', worker : {id : 1, busy : 1}});
});

Work.on('stop', done=>{
    client.send('master', {type: 'free', worker: {id : 1, busy: 0}, doneTasks : done});
});

Work.on('crash', () => {
    client.send('master', {type: 'crash', worker: {id : 1}});
});




