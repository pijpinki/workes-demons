let EventEmitter = require('events');

class Mq extends EventEmitter{
    constructor(){
        super();

        this.workers = [
            {id : 1, status : 1, busy : 0, remeaning :0},
            {id : 2, status : 1, busy : 0, remeaning :0},
            {id : 3, status : 1, busy : 0, remeaning :0},
            {id : 4, status : 1, busy : 0, remeaning :0}
        ];

        this.started = [1,2,3];
        this.busy    = [2];


        setTimeout(this.startMqService.bind(this),1000);
        // setTimeout(this.startWorkers.bind(this), 10000);
    }

    startMqService(){
        this.emit('started');
    }

    startWorkers(){
        this.workers.forEach(w => {
            this.emit('message', {type : 'started', worker : w});
        });

        this.crashWorkers();
        this.makeWorkersBusy();
    };

    crashWorkers(){
        for(let i = 0; i < this.workers.length; i++){
            let w    = this.workers[i];
            let find = false;

            for(let j = 0; j < this.started.length; j++){
                if(w.id === this.started[j]){
                    find = true;
                    break;
                }
            }

            if(!find) {
                w.status = 0;
                this.emit('message', {type : 'crash', worker: w})
            }
        }
    };

    makeWorkersBusy(){
        for(let i = 0; i < this.workers.length; i++){
            let w    = this.workers[i];
            let find = false;

            for(let j = 0; j < this.busy.length; j++){
                if(w.id === this.busy[j]){
                    find = true;
                    break;
                }
            }

            if(find){
                w.busy = 1;
                this.emit('message', {type : "busy", worker : w});
            }
        }
    };

    send(to, message){
        to === 'master' ? to = 'message' : 0;
        this.emit(to, message);
    };

    subscribe(chanel){

    }
}
let mq = new Mq();

console.log('new mq');

module.exports.createClient = function (ops) {
    return mq;
};