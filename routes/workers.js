let express = require('express');
let router  = express.Router();

let Send    = require('../modules/send');
let Workers = require('../modules/workersV2');

router.get('/status', (req, res)=>{
    Workers(W => Send.success(res,200, W.getStatus()));
});

module.exports = router;