let express = require('express');
let router  = express.Router();
let Send    = require('../modules/send');
let Workers = require('../modules/workersV2');
let Tasks   = require('../database/tasks');
let Async   = require('async');

router.get('/', (req, res)=>{
    Tasks.getTasks({page : req.query.page}, (err,data) => Send.full(res, err,data));
});

router.post('/', (req, res) => {
    let inputData = req.body.input;

    if(!inputData) {
        Send.error(res, 503, "No data");
    }else {
        Tasks.addTask({input_data : inputData}, (err, data) =>{
            Send.full(res, err, data);
            if(!err) {
                Workers(W => {
                    W.addTask({id: data.insertId, input_data: inputData});
                });
            }
        });
   }
});

router.post('/multy', (req, res) => {
    let inputData = req.body.input;

    if(!inputData){
        Send.error(res, 503, "No data");
    }else {
        let tasks = JSON.parse(inputData);

        function oneTask(task, callback){
            Tasks.addTask({input_data : JSON.stringify(task)}, (err,data)=> {
                if(err) callback(err);
                else {
                    Workers(W => W.addTask({id:data.insertId, input_data : JSON.stringify(task)}));
                    callback(null, true);
                }
            });
        }

        Async.map(tasks, oneTask, (err,done)=>Send.full(res,err,done));
    }
});

router.put('/', (req, res)=> {
   let task_id = req.body.task_id;
   if(!task_id){Send.error(res, 503, "No params");}else {
       Send.error(res, 503, "In debug");
   }
});

router.delete('/', (req, res) => {
    let id = req.body.id;

    if(!id) {
        Send.error(res, 503, "no id");
    }else {
        Tasks.deleteTask({id : id}, (err ,data) => Send.full(res, err,data));
    }
});

module.exports = router;