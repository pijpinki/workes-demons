#api [http://localhost:3000]
## Tasks [/api/tasks]
> Get tasks **GET**
+ Request
```
{
    token : "open"
}
```
+ Response success
```
{
    code : 200,
    data : [
        {
            id          : int,
            created_at  : timestamp,
            started_at  : timestamp,
            finished_at : timestamp,
            output_data : json,
            input_data  : json
        }
    ]
}
```

+ Response error
```
{
    code : 403 | 503,
    data : { message : "Error message" }
}
```



> Add task **POST**
+ Request

```
{
    token : "open",
    input : {"value" : 1}    
}
```

+ Response success
```
{
    code : 200,
    data : {
        inserTd : int
    }
}
```

+ Response error
```
    code : 403 | 503,
    data : { message : string }
```

> Add many task **POST** /multy
+ Request

```
{
    token : "open",
    input : [{"value" : 1},...]
}
```

+ Response success
```
{
    code : 200,
    data : {}
}
```

+ Response error
```
    code : 403 | 503,
    data : { message : string }
```

> Remove task **DELETE**

+ Request
```
{
    token : "open",
    id    : int
}
```

+ Response success
```
{
    code : 200
    data : {
        fieldCount    : int,
        affectedRows  : int,
        insertId      : int,
        serverStatus  : int,
        warningCount  : int,
        message       : string,
        protocol41    : bool,
        changedRows   : int       
    }
}
```

+ Response error 
```
{
    "code": 503,
    "data": {
        "message": string
    }
}
```

## Workers [/api/workers]

> Get status of all workers **GET**

+ Request
```
{
    token : "open"
}
```

+ Response success 
```
{
    code : 200,
    data : {
        [ { id: int, status: 1 | 0, busy: 1 | 0, remeaning: int } ] }
    }
}
```

+ Response error
```
{
    code : 403 | 503,
    data : { message : string }
}
```