let needle = require('needle');
let Async  = require('async');

let url = "http://127.0.0.1:3000";




describe('tsks', function () {
    let added = [];
    this.timeout(5000);
    describe('add', () => {

        it('return okay task added', done => {

            needle.post(url + '/api/tasks', {token : 'open', input : JSON.stringify({value : 1})}, (err, data) =>{

                if(err) done(err);
                else if(data.body.code === 200) {
                    added.push(data.body.data.insertId);
                    done()
                }else {
                    done(data.body.data);
                }
            })

        });

        it('return error no data', done => {

            needle.post(url + '/api/tasks', {token : 'open'}, (err, data) => {

                if(err) done(err);
                else {
                    data = data.body;
                    data.code === 200 ? done(data.body.data) : done();
                }

            })

        });

        it('return erorr no token', done => {

            needle.post(url + 'api/tasks', {}, (err, data) =>{

                if(err) done(err);
                else {
                    data = data.body;
                    data.code === 200 ? done(data.data) : done();
                }

            })

        });
    });

    describe('get', () => {

        it('return no error', done => {

            needle.get(url + '/api/tasks?token=open', (err, data) => {

                if(err) done(err);
                else {
                    data = data.body;
                    data.code === 200 ? done() : done(data.data);
                }

            });

        });

        it('return error no token', done => {

            needle.get(url + '/api/tasks', (err, data) => {

                if(err) done(err);
                else {
                    data = data.body;
                    data.code === 200 ? done(data.data) : done();
                }

            })

        })
    });

    describe('delete', () => {


        it('return okay', done => {

            function del(id, callback) {
                needle.delete(url + '/api/tasks', {id : id, token : "open"}, (err, data) => {
                    if(err) done (err);
                    else {
                        data = data.body;
                        data.code === 200 ? callback() : callback(data.data);
                    }
                });
            }

            setTimeout(() => {Async.map(added, del, done)},1000);
        });

        it('return error', done => {

           needle.delete(url + '/api/tasks', {id : -1}, (err, data)=>{

               if(err) done(err);
               else {
                   data = data.body;
                   data.code === 200 ? done(data.data) : done();
               }

           });

        });
    });
});

describe('workers', function () {
    this.timeout(10000);
   let w_url = url + "/api/workers/status?token=open";

   describe('Get status of workers', () => {

       it('return data', done => {

           needle.get(w_url, (err, res) => {
               if(err) done(err);
               else {
                   res = res.body;
                   res.code === 200 ? done() : done(res.data);
               }
           })

       })

   });
});

let async  = require('async');

 url = "http://127.0.0.1:3000";
describe('Tasks', function() {
    this.timeout(10**4);
    let taskUrl = url + "/api/tasks";
    let workerUrl = url + "/api/workers/status?token=open";

    describe.skip('Start task', ()=>{
        it('Return okay', done => {
            needle.post(taskUrl, {token : "open", input : JSON.stringify({value : 18})}, (err, data)=>{
                if(err) done(err);
                else {
                    data = data.body;
                    data.code === 200 ? done() : done(data);
                }
            })
        });
    });

    describe('load test', ()=>{
        this.timeout = 10000;

        let count = new Array(500);

        function go(c,callback) {
            needle.post(taskUrl, {token : 'open', input : JSON.stringify({value : Math.random() * 10})}, (err,data) => {
                if(err) callback(err);
                else {
                    data = data.body;
                    data.code === 200 ? callback() : callback(data.data);
                }
            });
        }

        it('show status free', done => {
            needle.get(workerUrl, (err, data) => {
                if(err) done(err);
                else {
                    data = data.body;
                    console.log(data);
                    data.code === 200 ? done() : done(data.data);
                }
            });
        });

        it('Return okay', done => {
            async.map(count, go, done)
        });

        it('show status busy', done => {
            needle.get(workerUrl, (err, data) => {
                if(err) done(err);
                else {
                    data = data.body;
                    console.log(data);
                    data.code === 200 ? done() : done(data.data);
                }
            });
        })
    })
});

describe.skip('load test', function () {
    this.timeout(50000);
    let maxThreads = new Array(2000);

    for(let i = 0; i < maxThreads.length; i++){
        maxThreads[i] = Math.random();
    }

    describe('2000 start', function () {
        it('return okay', function (done) {

            function start(value, callback) {
                needle.post('http://localhost:3000/api/tasks', {token : "open", input: JSON.stringify({value : value})}, (err, data) => {
                    if(err) callback(err);
                    else {
                        data = data.body;
                        data.code === 200 ? callback() : callback(data.data || err || "error");
                    }
                });
            }

            Async.map(maxThreads, start, done);
        })

    });
});