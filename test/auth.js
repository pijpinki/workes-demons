let auth = require('../modules/auth');

let modal = {
    domain : "",
    req : {
        originalUrl : "",
        get  : function () {
            return modal.domain
        },
        body : {
            token : "open"
        },
        query : {
            token : "open"
        }
    },
    res : {
        send : null
    }
};

describe('Auth', function() {
    describe('Check fo open page with token', function() {
        it('Return okay', function(done) {
            modal.domain          = "localhost:3000";
            modal.req.originalUrl = "/api/images";
            modal.res.send        = function (data) {
                done(data);
            };

            auth(modal.req, modal.res, done);
        });
    });

    describe('Check fo open page without token', function() {
        it('Return error', function(done) {
            modal.domain          = "localhost:3000";
            modal.req.originalUrl = "/api/images?token=lol";
            modal.res.send        = data => data ? done () : done("Error : no error");
            modal.req.query.token = "lol";

            auth(modal.req, modal.res, done);
        });
    });

    describe('Check private pages local domain with open token', function() {
        it('Return okay', function(done) {
            modal.domain          = "localhost:3000";
            modal.req.originalUrl = "/api/work";
            modal.req.query.token = "open";
            modal.res.send        = data => done(data);
            auth(modal.req, modal.res, done);
        });
    });

    describe('Check private pages local domain without open token', function() {
        it('Return error', function(done) {
            modal.domain          = "localhost:3000";
            modal.req.originalUrl = "/api/work";
            modal.req.query.token = "lol";
            modal.res.send        = data => data ? done() : done("Error no data");
            auth(modal.req, modal.res, done);
        });
    });

    describe('Check private pages not alowed domain', function() {
        it('Return error', function(done) {
            modal.domain          = "medtales.com";
            modal.req.originalUrl = "/api/work";
            modal.req.query.token = "open";
            modal.res.send        = data => data ? done() : done("Error no data");
            auth(modal.req, modal.res, done);
        });
    });

});