let fs = require('fs');

module.exports.getConfig = function () {
    return JSON.parse(fs.readFileSync('./config.json'));
};