/**
 * Send responce error
 * @param res - express response object
 * @param {int} code - Error code
 * @param {string} message - Error message
 */
module.exports.error = function (res, code, message) {
    res.send({code : code, data : {message : message}});
};

/**
 * Send response with data
 * @param res - express response object
 * @param {int} code - message code
 * @param data - data to send
 */
module.exports.success = function (res, code, data) {
    res.send({code : code, data : data});
};

/**
 * Router error sucess
 * @param res - express object
 * @param err - error object
 * @param data - success object
 */
module.exports.full = function (res, err, data) {
    err ? this.error(res, 503, err) : this.success(res, 200, data);
};