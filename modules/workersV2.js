let amqp    = require('amqplib/callback_api');
let _name   = "master";
let _global = "Global";
let _tasks  = "tasks";
let WorkerClass = null;

amqp.connect('amqp://lmkxmovw:EHJyzCUF5WoSY6RK4e-DO5rBEk17HVed@lark.rmq.cloudamqp.com/lmkxmovw', function(err, conn) {
    if(!err) {
        conn.createChannel(function (err, ch) {
            console.log('chanel', err);
            // ch.assertQueue(q, {durable: false});
            // Note: on Node 6 Buffer.from(msg) should be used

            WorkerClass = new Worker(conn, ch);
            WorkerClass.start();
        });
    }else {throw err}
});

class Worker {
    constructor(conn, ch){
        this.workers = {};
        this.workers2= {};
        this.tasks   = [];
        this.conn    = conn;
        this.ch      = ch;
    }

    start(){
        this.ch.assertQueue(_global, {durable: false});
        this.ch.assertQueue(_name,   {durable: false});
        this.ch.assertQueue(_tasks,  {durable: true});
        this.ch.consume(_name, this.revice.bind(this), {noAck: true});

        setInterval(this.clear.bind(this), 3000);
    }


    revice(input){
        let data= null;
        if(input.content.toString().indexOf('{') !== -1)
            data = JSON.parse(input.content.toString());
        else
            data = input.content.toString();

        if(typeof (data) === 'object') {
            if (data.type === "status") {
                this.workers[data.data.id] = data.data;
                this.workers2[data.data.id] = data.data;
            }
        }
    }

    send(type, data){
        let send = new Buffer(JSON.stringify(data));

        if(type === 'task'){
            this.ch.sendToQueue(_tasks, send);
        }
        if(type === 'status'){
            this.ch.sendToQueue(_global, send, {durable : false})
        }
    }

    addTask(task){
        this.send('task', task);
    }


    getStatus(){
        return this.workers;
    }

    clear(){
        this.workers2 = {};

        setTimeout(()=> this.workers = this.workers2, 1200);
    }
}

module.exports = function (callback) {

    check();

    function check() {
        if(!WorkerClass) setTimeout(check,100);
        else callback(WorkerClass);
    }

};