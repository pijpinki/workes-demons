let Config     = require('./config').getConfig();
let mqlight    = require('../sumilators/mq');
let Tasks      = require('../database/tasks');


let recvClient = mqlight.createClient({service:'amqp://localhost'} , err => console.log(err));

let _workers = {};
let _tssks   = {};
let _W       = null;

recvClient.on('started', function() {

    console.log(`Mq started`);

    recvClient.subscribe('master');

    recvClient.on('message', function(data, delivery) {
        if(data.type === "started"){
            _workers[data.worker.id] = data.worker;  console.log(`Worker ${data.worker.id} registred`);
        }
        if(data.type === "crash"){
            _workers[data.worker.id].staus = 0; startAgin();
        }
        if(data.type === "busy"){
            _workers[data.worker.id].busy = 1; console.log(`Worker ${data.worker.id} is busy`);
        }
        if(data.type === "free"){
            _workers[data.worker.id].busy = 0; removeDone(data.doneTasks);
        }
        if(data.type === "remeaning"){
            _workers[data.worker.id].remeaning = data.worker.remeaning;
        }
        if(data.type === "state"){
            _workers[data.worker.id].busy      = data.worker.busy;
            _workers[data.worker.id].remeaning = data.worker.remeaning;
        }
    });

    recvClient.send('global', {type : 'status'});
});

function removeDone(taskList) {
    console.log('tasks done');
    taskList.forEach(task => {
        Tasks.setFinish({id : task.id});
        delete _tssks[task.id];
    });
}

function startAgin() {
    _W ? 0 : _W = new Workers();
    _W.start();
}

class Workers {
    constructor () {

    }

    start(task, callback)
    {
        task ? _tssks[task.id] = task : 0;

        let online = this.getAll();
        let ab     = this.checkWhoAvalible(online);

        if(Object.keys(online).length) {
            if (ab.avalible.length > 0) {
                this.addToQuery(online[Object.keys(online)[0]]);
            } else {
                // find where query
                let free = this.findFree(ab.busy);
                this.addToQuery(free[0]);
            }
        }else {
            setTimeout(this.start.bind(this), 10000);
        }
    }

    checkWhoAvalible(online){
        let avalible = [];
        let busy     = [];

        Object.keys(online).forEach(key => {
            !_workers[key].busy ? avalible.push(_workers[key]) : busy.push(_workers[key]);
        });

        return {avalible : avalible, busy : busy};
    }

    getAll(){
        let online = {};
        Object.keys(_workers).forEach(key => _workers[key].status ? online[key] = _workers[key] : 0);
        return online;
    }

    findFree(busy){
        let numbers = [];

        busy.forEach(b => numbers.push(b.remeaning));

        bubbleSort();
        return busy;

        function bubbleSort()
        {
            let swapped;
            do {
                swapped = false;
                for (let i = 0; i < numbers.length-1; i++) {
                    if (numbers[i] > numbers[i+1]) {
                        let temp  = numbers[i];
                        let temp2 = busy[i] ;

                        numbers[i]   = numbers[i+1];
                        numbers[i+1] = temp;

                        busy[i]   = busy[i+1];
                        busy[i+1] = temp2;

                        swapped = true;
                    }
                }
            } while (swapped);
        }
    }

    addToQuery(worker){
        let toSend = this.getFreeTaska();
        toSend.length > 0 ? recvClient.send(worker.id, {type: 'query', data : toSend }) : 0;
    }

    getFreeTaska(){
        let taska = [];
        Object.keys(_tssks).forEach(key => {
            !_tssks[key].started ? taska.push(_tssks[key]) : 0;
        });
        return taska;
    }

    static getStatus(callback){
        recvClient.send('global', {type : 'state'});

        setTimeout(()=>{
            let out = [];
            Object.keys(_workers).forEach(k => out.push(_workers[k]));
            callback(null, out);
        },5000);
    }
}


module.exports = Workers;
