let fs = require('fs');

let config = JSON.parse(fs.readFileSync('./auth.json'));

module.exports = function (req, res, next) {
    let token    = req.query.token || req.body.token || req.header.token;
    let domain   = req.get('host');
    let addess   = req.originalUrl;
    let error    = "";
    let pageType = "";

    // console.log('auth module');
    // console.log(`token = ${token} domain = ${domain} config = ${config} address = ${addess}`);

    // check pricate pages
    config.privatePages.forEach(pPage => {
       if(addess.indexOf(pPage) !== -1){
           pageType = "private";
       }
    });

    // check open pages
    config.openPages.forEach(page => {
        if(addess.indexOf(page) !== -1) pageType = "open";
    });

    // check authPages
    config.authPages.forEach(page => {
        if(addess.indexOf(page) !== -1) pageType = "auth";
    });


    // check token
    if(pageType === "private"){
        // check for CORS
        if(!checkPages()) error += "CORS not allowed";
        if(!error) token !== config.openToken ? error += "Access dennie" : 0;
    }else if(pageType === "open") {
        // check for open
        if (token !== config.openToken) error += "Access dennie";
    }else if(pageType === "auth"){
        // todo check token from database
    }else {}

    // console.log(`PAGE TYPE ${pageType}`);

    if(!error) {
        next();
    }else {
        res.send({code : 403, data : { message : error}});
    }


    function checkPages() {
        let find = false;
        for(let i = 0; i < config.alowedDomains.length; i++){
            if(domain.indexOf(config.alowedDomains[i]) !== -1){
                find = true;
                break;
            }
            if(config.alowedDomains[i].indexOf(domain) !== -1){
                find = true;
                break;
            }
        }

        return find;
    }


    function checkToken(){

    }
};