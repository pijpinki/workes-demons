let mysql = require('./connect');

module.exports.addTask = function (params, callback) {
    let sql = "INSERT INTO tasks (`input_data`) VALUES (?)";
    mysql.query(sql, [params.input_data], callback);
};

module.exports.getTasks = function (params, callback) {
    let sql = "";
    params.page ? sql = `SELECT * FROM tasks ORDER BY \`tasks\`.\`id\` DESC LIMIT ${params.page * 20}, 20` :
        sql = "SELECT * FROM tasks ORDER BY `tasks`.`id` DESC";
    console.log(sql);
    mysql.query(sql, callback);
};

module.exports.deleteTask = function (params, callback) {


    function checkForExist(callback) {
        let sql = "SELECT * FROM tasks WHERE id = ? AND finished_at <> 0";
        mysql.query(sql, [params.id], callback);
    }

    function remove(err,data) {
        if(err || !data.length) callback(err || "Not found or busy");
        else {
            let sql = "DELETE from tasks WHERE id = ?";
            mysql.query(sql, [params.id], callback)
        }
    }

    checkForExist(remove);
};

/**
 * Set finish data
 * @param params
 * @param params.ids
 * @param callback
 */
module.exports.setFinish = function (params, callback) {
    let slq = "UPDATE `tasks` SET `finished_at`=CURRENT_TIMESTAMP() WHERE id IN (?)";
    mysql.query(slq, [params.ids], callback);
};

/**
 * Set Started data
 * @param params
 * @param params.ids
 * @param callback
 */
module.exports.setStarted = function (params, callback) {
    let sql = "UPDATE `tasks` SET `started_at`=CURRENT_TIMESTAMP() WHERE id IN (?)";
    mysql.query(sql, [params.ids], callback)
};

/**
 * Set output_data
 * @param params
 * @param params.id
 * @param params.output_data
 * @param callback
 */
module.exports.setOutput = function (params, callback) {
    let sql = "UPDATE tasks SET `output_data`=?, `finished_at`=CURRENT_TIMESTAMP() WHERE id =?";
    mysql.query(sql, [params.output_data, params.id], callback);
};