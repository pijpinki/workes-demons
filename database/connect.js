let mysql  = require('mysql');
let config = require('../modules/config').getConfig();

module.exports = mysql.createPool(config.database);
